﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp2_lesson4
{
    public partial class NumberGuess : Form
    {
        private int _minimumNumber = 1;
        private int _maximumNumber = 10;
        private int _targetNumber = 0;
        private Random _randomNumber = new Random();
        public NumberGuess()
        {
            InitializeComponent();
            _targetNumber = _randomNumber.Next(_minimumNumber, _maximumNumber);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int num = Convert.ToInt32(numberbox.Text);
            
            if (num == _targetNumber)
            {
                outcomelabel.Text = "Correct";
            }
            else if (num < _targetNumber)
            {
                outcomelabel.Text = "too small";
            }
            else
            {
                outcomelabel.Text = "too large";
            }
        }

        private void restartbutton_Click(object sender, EventArgs e)
        {
            numberbox.Text = "";
            _targetNumber = _randomNumber.Next(_minimumNumber, _maximumNumber);
            outcomelabel.Text = "";
        }

        private void exitbutton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
